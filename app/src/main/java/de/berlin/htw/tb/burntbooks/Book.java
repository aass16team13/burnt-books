package de.berlin.htw.tb.burntbooks;

/**  The Database is comprised of a single node containing all books
 *   [ Stands for a JSONArray
 *   { Stands for a JSONObject
 *   */
public class Book {

    private String title;

    public void setSsFlag(Boolean ssFlag) {
        this.ssFlag = ssFlag;
    }

    private Boolean ssFlag;
    private String authorFirstName;
    private String authorLastName;
    private String firstEditionPublisher;
    private String firstEditionPublicationYear;
    private String firstEditionPublicationPlace;
    private String secondEditionPublisher;
    private String secondEditionPublicationYear;
    private String secondEditionPublicationPlace;
    private String additionalInfos;
    private String pageNumberInOCRDocument;
    private String ocrResult;
    private String correctionsAfter1938;

    public Book() {

    }

    public Book(String title, String authorFirstName) {
        this.title = title;
        this.authorFirstName = authorFirstName;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthorFirstName(String authorFirstName) {
        this.authorFirstName = authorFirstName;

    }

    public void setAuthorLastName(String authorLastName) {
        this.authorLastName = authorLastName;
    }

    public void setFirstEditionPublisher(String firstEditionPublisher) {
        this.firstEditionPublisher = firstEditionPublisher;
    }

    public void setFirstEditionPublicationYear(String firstEditionPublicationYear) {
        this.firstEditionPublicationYear = firstEditionPublicationYear;
    }

    public void setFirstEditionPublicationPlace(String firstEditionPublicationPlace) {
        this.firstEditionPublicationPlace = firstEditionPublicationPlace;
    }

    public void setSecondEditionPublisher(String secondEditionPublisher) {
        this.secondEditionPublisher = secondEditionPublisher;
    }

    public void setSecondEditionPublicationYear(String secondEditionPublicationYear) {
        this.secondEditionPublicationYear = secondEditionPublicationYear;
    }

    public void setSecondEditionPublicationPlace(String secondEditionPublicationPlace) {
        this.secondEditionPublicationPlace = secondEditionPublicationPlace;
    }

    public void setAdditionalInfos(String additionalInfos) {
        this.additionalInfos = additionalInfos;
    }

    public void setPageNumberInOCRDocument(String pageNumberInOCRDocument) {
        this.pageNumberInOCRDocument = pageNumberInOCRDocument;
    }

    public void setOcrResult(String ocrResult) {
        this.ocrResult = ocrResult;
    }

    public void setCorrectionsAfter1938(String correctionsAfter1938) {
        this.correctionsAfter1938 = correctionsAfter1938;
    }

    public Book(String title, Boolean ssFlag, String authorFirstName, String authorLastName,
                String firstEditionPublisher, String firstEditionPublicationYear,
                String firstEditionPublicationPlace, String secondEditionPublisher,
                String secondEditionPublicationYear, String secondEditionPublicationPlace,
                String additionalInfos, String pageNumberInOCRDocument, String ocrResult,
                String correctionsAfter1938) {
        if (title == null || authorLastName == null)
            throw new IllegalArgumentException();
        else
        {
            this.title = title;
            this.ssFlag = ssFlag;
            this.authorFirstName = authorFirstName;
            this.authorLastName = authorLastName;
            this.firstEditionPublisher = firstEditionPublisher;
            this.firstEditionPublicationYear = firstEditionPublicationYear;
            this.firstEditionPublicationPlace = firstEditionPublicationPlace;
            this.secondEditionPublisher = secondEditionPublisher;
            this.secondEditionPublicationYear = secondEditionPublicationYear;
            this.secondEditionPublicationPlace = secondEditionPublicationPlace;
            this.additionalInfos = additionalInfos;
            this.pageNumberInOCRDocument = pageNumberInOCRDocument;
            this.ocrResult = ocrResult;
            this.correctionsAfter1938 = correctionsAfter1938;
        }
    }

    public String getTitle() {
        return title;
    }

    public String getTitle(int len) {
        if (title.length() > len)
            return title.substring(0, len - 1) + "..";
        return title;
    }

    public Boolean getSsFlag() {
        return ssFlag;
    }

    public String getAuthor(int len) {
        String result = authorLastName;
        if (authorFirstName.length() > 0)
             result += ", " + authorFirstName.substring(0, 1);
        if (result.length() > len)
            return result.substring(0, len) + "..";
        return result;
    }

    public String getAuthorFirstName() {
        return authorFirstName;
    }

    public String getAuthorLastName() {
        return authorLastName;
    }

    public String getFirstEditionPublisher() {
        return firstEditionPublisher;
    }

    public String getFirstEditionPublicationYear() {
        return firstEditionPublicationYear;
    }

    public String getFirstEditionPublicationPlace() {
        return firstEditionPublicationPlace;
    }

    public String getSecondEditionPublisher() {
        return secondEditionPublisher;
    }

    public String getSecondEditionPublicationYear() {
        return secondEditionPublicationYear;
    }

    public String getSecondEditionPublicationPlace() {
        return secondEditionPublicationPlace;
    }

    public String getAdditionalInfos() {
        return additionalInfos;
    }

    public String getPageNumberInOCRDocument() {
        return pageNumberInOCRDocument;
    }

    public String getOcrResult() {
        return ocrResult;
    }

    public String getCorrectionsAfter1938() {
        return correctionsAfter1938;
    }
}
