package de.berlin.htw.tb.burntbooks;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class BookList {


    public static List<Book> books = new ArrayList<Book>();

    private BookList() {

    }

    public static void clearBooks() {
        books = new ArrayList<Book>();
    }

    public static void addBook(Book newBook) {
        books.add(newBook);
    }

    private static int getBooksLen() {
        return books.size();
    }
}
