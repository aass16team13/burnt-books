package de.berlin.htw.tb.burntbooks;

import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by tb on 14.07.16.
 */
public class FavoritesList {

    public static List<Book> books = new ArrayList<Book>();
    public static final String debugTag = "FavoritesList.DEBUG";

    private FavoritesList() {

    }

    public static void removeBook(Book removeBook) {
        for (Iterator<Book> iter = books.listIterator(); iter.hasNext(); ) {
            Book tmp = iter.next();
            if (tmp.getTitle() == removeBook.getTitle() &&
                    tmp.getAuthorLastName() == removeBook.getAuthorLastName()) {
                iter.remove();
            }
        }
    }

    public static void clearBooks() {
        books = new ArrayList<Book>();
    }

    public static void addBook(Book newBook) {
            if (containsBook(newBook))
                Log.d(debugTag, newBook.getTitle());
            else
                books.add(newBook);

    }

    public static Boolean containsBook(Book checkBook) {

        Boolean result = false;
        Iterator<Book> iter = books.listIterator();
        while (iter.hasNext()) {
            Book tmp = iter.next();
            if (tmp.getTitle().equals(checkBook.getTitle()) &&
                    tmp.getAuthorLastName().equals(checkBook.getAuthorLastName())) {
                result = true;
            }
            else {
                Log.d(debugTag, tmp.getTitle() + ":" + checkBook.getTitle());
            }
        }
        return result;
    }

    public static List<Book> getBooks() {
        return books;
    }

    public static void replaceBooks(List<Book> newBooks) {
        books = newBooks;
    }

    private static int getBooksLen() {
        return books.size();
    }
}
