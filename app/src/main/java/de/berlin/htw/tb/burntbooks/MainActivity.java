package de.berlin.htw.tb.burntbooks;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.os.EnvironmentCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ItemFragment.OnListFragmentInteractionListener,
        ShowBookFragment.OnFragmentInteractionListener, FavoriteFragment.OnListFragmentInteractionListener {

    private static final String MODE = "OFFLINE";
    private String debugTag;
    private Button btnBrowseBooks;
    private Button btnBrowseFavorites;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        debugTag = getString(R.string.debug_tag_main);
        btnBrowseBooks = (Button) findViewById(R.id.btn_browse_books);
        btnBrowseFavorites = (Button) findViewById(R.id.btn_browse_favorites);

        btnBrowseBooks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleButtonClick("browseBooks");
            }
        });

        btnBrowseFavorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleButtonClick("browseFavorites");
            }
        });

        loadFavoritesList();
        BookList.clearBooks();
        //createFakeDataset();
        //setFragment(savedInstanceState);
        //new Downl oadFilesTask().execute();


    }

    private void startFragment(Fragment fragment) {
        setFragment(new Bundle(), fragment);
    }

    private void handleButtonClick(String token) {
        switch(token) {
            case ("browseBooks"):
                startFragment(new ItemFragment());
                new DownloadFilesTask().execute(); //Only once
                break;
            case ("browseFavorites"):
                startFragment(FavoriteFragment.newInstance());
                break;
            default:
                break;
        }
    }

    private void createFakeDataset() {
        Book book = new Book();
        book.setTitle(getString(R.string.fake_title_1));
        book.setAuthorFirstName(getString(R.string.fake_author_first_1));
        book.setAuthorLastName(getString(R.string.fake_author_last_1));
        book.setAdditionalInfos(getString(R.string.fake_add_info));
        BookList.addBook(book);
        book = new Book();
        book.setTitle(getString(R.string.fake_title_2));
        book.setAuthorFirstName(getString(R.string.fake_author_first_2));
        book.setAuthorLastName(getString(R.string.fake_author_last_2));
        book.setAdditionalInfos(getString(R.string.fake_add_info));
        BookList.addBook(book);
    }

    private void setFragment(Bundle savedInstanceState, Fragment fragment) {
        if (findViewById(R.id.fragment_container) != null) {
            /*if (savedInstanceState != null)
                return;*/

            //ItemFragment itemFragment = new ItemFragment();
            //getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).commit();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        }
    }

    public void loadFavoritesList() {
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = sharedPreferences.getString("FavoritesList", "");

        Type type = new TypeToken<List<Book>>(){}.getType();
        List<Book> books = gson.fromJson(json, type);

        FavoritesList.replaceBooks(books);
    }

    public void saveFavoritesList() {
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(FavoritesList.getBooks());
        editor.putString("FavoritesList", json);
        editor.commit();
    }

    @Override
    public void onListFragmentInteraction(Book item, String token) {
        if (token == "bookmark") {
            FavoritesList.addBook(item);

            saveFavoritesList();
            FavoriteFragment favoriteFragment = FavoriteFragment.newInstance();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, favoriteFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        }
        if (token == "showbook") {
            ShowBookFragment showBookFragment = ShowBookFragment.newInstance(item.getTitle(), item.getAuthorFirstName(),
                    item.getSsFlag(), item.getAuthorLastName(), item.getFirstEditionPublisher(), item.getFirstEditionPublicationYear(),
                    item.getFirstEditionPublicationPlace(), item.getSecondEditionPublisher(),
                    item.getSecondEditionPublicationYear(), item.getSecondEditionPublicationPlace(),
                    item.getAdditionalInfos(), item.getPageNumberInOCRDocument(),
                    item.getOcrResult(), item.getCorrectionsAfter1938());

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, showBookFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            showBookFragment.updateView(item);
        }
        if (token == "delete") {
            FavoritesList.removeBook(item);
            saveFavoritesList();
            FavoriteFragment favoriteFragment = FavoriteFragment.newInstance();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, favoriteFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    private class DownloadFilesTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Boolean doInBackground(Void... voids) {
            try {
                InputStream inputStream;
                Reader reader;
                if (MODE == "ONLINE") {
                    URL url = new URL(getString(R.string.database_url));
                    Log.d(debugTag, getString(R.string.database_url));
                    URLConnection urlConnection = (URLConnection) url.openConnection();
                    urlConnection.connect();
                    inputStream = new BufferedInputStream(url.openStream());
                    reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 2048);
                }
                if (MODE == "OFFLINE") {
                    inputStream = new BufferedInputStream(getResources().openRawResource(R.raw.verbannte_buecher));
                    reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 2048);
                }
                else {
                    reader = new FileReader("/home/tb/Downloads/verbannte-buecher.json");
                }

                JsonReader jsonReader = new JsonReader(reader);
                Book book;

                try {

                    jsonReader.beginArray();
                    BookList.clearBooks();
                    while (jsonReader.hasNext()) {
                        book = new Book();
                        jsonReader.beginObject();
                        while (jsonReader.hasNext()) {
                            String name = jsonReader.nextName();

                            switch (name) {
                                case ("title"): {
                                    book.setTitle(jsonReader.nextString());
                                    break;
                                }
                                case ("authorFirstname"): {
                                    book.setAuthorFirstName(jsonReader.nextString());
                                    break;
                                }
                                case ("authorLastname"): {
                                    book.setAuthorLastName(jsonReader.nextString());
                                    break;
                                }
                                case ("ssFlag"):
                                    book.setSsFlag(jsonReader.nextBoolean());
                                    break;
                                case ("firstEditionPublisher"):
                                    book.setFirstEditionPublisher(jsonReader.nextString());
                                    break;
                                case ("firstEditionPublicationYear"):
                                    book.setFirstEditionPublicationYear(jsonReader.nextString());
                                    break;
                                case ("firstEditionPublicationPlace"):
                                    book.setFirstEditionPublicationPlace(jsonReader.nextString());
                                    break;
                                case ("secondEditionPublisher"):
                                    book.setSecondEditionPublisher(jsonReader.nextString());
                                    break;
                                case ("secondEditionPublicationYear"):
                                    book.setSecondEditionPublicationYear(jsonReader.nextString());
                                    break;
                                case ("secondEditionPublicationPlace"):
                                    book.setSecondEditionPublicationPlace(jsonReader.nextString());
                                    break;
                                case ("additionalInfos"):
                                    book.setAdditionalInfos(jsonReader.nextString());
                                    break;
                                case ("pageNumberInOCRDocument"):
                                    book.setPageNumberInOCRDocument(jsonReader.nextString());
                                    break;
                                case ("ocrResult"):
                                    book.setOcrResult(jsonReader.nextString());
                                    break;
                                case ("correctionsAfter1938"):
                                    try {
                                        //jsonReader.skipValue();
                                        String tmp = jsonReader.nextString();
                                        if (tmp != null)
                                            book.setCorrectionsAfter1938(tmp);
                                    }
                                    catch (IllegalStateException ise) {
                                        jsonReader.skipValue();
                                    }
                                    break;
                                default:
                                    try {
                                        jsonReader.skipValue();
                                    }
                                    catch (IllegalStateException ise) {

                                    }
                                    //book.add(name, jsonReader.nextString());
                                    break;
                            }
                        }
                        jsonReader.endObject();
                        BookList.addBook(book);
                    }
                    jsonReader.endArray();
                }
                finally {
                    jsonReader.close();
                    reader.close();
                }
                if (MODE == "ONLINE")
                    inputStream.close();



            }
            catch (MalformedURLException mue) {
                mue.printStackTrace();
            }
            catch (IOException ioe) {
                ioe.printStackTrace();
            }
            return true;
        }


        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            Log.d(debugTag, "I was reached!!");


            if (getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof ItemFragment) {
                ItemFragment itemFragment = (ItemFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                itemFragment.update();
            }


          }
    }

}
