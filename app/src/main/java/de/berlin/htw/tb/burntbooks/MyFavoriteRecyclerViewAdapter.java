package de.berlin.htw.tb.burntbooks;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import de.berlin.htw.tb.burntbooks.ItemFragment.OnListFragmentInteractionListener;



public class MyFavoriteRecyclerViewAdapter extends RecyclerView.Adapter<MyFavoriteRecyclerViewAdapter.ViewHolder> {

    private final List<Book> books;
    private final OnListFragmentInteractionListener mListener;

    public MyFavoriteRecyclerViewAdapter(List<Book> items, OnListFragmentInteractionListener listener) {
        books = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_favorite, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.book = books.get(position);
        holder.tvTitle.setText(books.get(position).getTitle(20));
        holder.tvAuthor.setText(books.get(position).getAuthor(20));


        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.book, "");
                }
            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onListFragmentInteraction(holder.book, "delete");
            }
        });

        holder.btnShowBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onListFragmentInteraction(holder.book, "showbook");
            }
        });
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public final TextView tvTitle;
        public final TextView tvAuthor;
        public ImageButton btnDelete;
        public ImageButton btnShowBook;
        public de.berlin.htw.tb.burntbooks.Book book;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            tvAuthor = (TextView) view.findViewById(R.id.tv_author);
            btnDelete = (ImageButton) view.findViewById(R.id.btn_delete);
            btnShowBook= (ImageButton) view.findViewById(R.id.btn_showbook);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvTitle.getText() + "'";
        }


        public View getView(int position, View view, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.fragment_favorite, parent, false);
            ImageButton button = (ImageButton) view.findViewById(R.id.btn_delete);




            return rowView;
        }
    }


}
