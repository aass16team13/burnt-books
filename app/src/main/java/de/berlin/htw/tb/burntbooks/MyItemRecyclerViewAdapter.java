package de.berlin.htw.tb.burntbooks;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import de.berlin.htw.tb.burntbooks.ItemFragment.OnListFragmentInteractionListener;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Book} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private final List<de.berlin.htw.tb.burntbooks.Book> books;
    private final OnListFragmentInteractionListener listener;


    public MyItemRecyclerViewAdapter(List<de.berlin.htw.tb.burntbooks.Book> items, OnListFragmentInteractionListener listener) {
        books = items;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }




    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.book = books.get(position);
        //holder.tvTitle.setText(books.get(position).getAuthorFirstName());
        //holder.tvAuthor.setText(books.get(position).getAuthorLastName());
        holder.tvTitle.setText(books.get(position).getTitle(20));
        holder.tvAuthor.setText(books.get(position).getAuthor(20));

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    listener.onListFragmentInteraction(holder.book, "listitem");
                }
            }
        });

        holder.btnBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onListFragmentInteraction(holder.book, "bookmark");
            }
        });

        holder.btnShowBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onListFragmentInteraction(holder.book, "showbook");
            }
        });
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public final TextView tvTitle;
        public final TextView tvAuthor;
        public ImageButton btnBookmark;
        public ImageButton btnShowBook;
        public de.berlin.htw.tb.burntbooks.Book book;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            tvTitle = (TextView) view.findViewById(R.id.tv_title);
            tvAuthor = (TextView) view.findViewById(R.id.tv_author);
            btnBookmark = (ImageButton) view.findViewById(R.id.btn_bookmark);
            btnShowBook = (ImageButton) view.findViewById(R.id.btn_showbook);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvTitle.getText() + "'";
        }


        public View getView(int position, View view, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.fragment_item, parent, false);
            ImageButton button = (ImageButton) view.findViewById(R.id.btn_bookmark);
            if (FavoritesList.containsBook(book))
                btnBookmark.setVisibility(View.INVISIBLE);

            return rowView;
        }
    }

}
