package de.berlin.htw.tb.burntbooks;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ShowBookFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ShowBookFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ShowBookFragment extends Fragment {

    private String titleString;

    private TextView title;
    private TextView author;
    private TextView ssFlag;
    private TextView firstPublisher;
    private TextView firstPublisherYear;
    private TextView firstPublisherPlace;
    private TextView secondPublisher;
    private TextView secondPublisherYear;
    private TextView secondPublisherPlace;
    private TextView addInfo;
    private TextView pageNrOcr;
    private TextView ocrResult;
    private TextView corrections;
    private Button btnShowInDnb;

    private OnFragmentInteractionListener mListener;

    public ShowBookFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ShowBookFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ShowBookFragment newInstance(String title, String authorFirstName, Boolean ssFlag, String authorLastName,
                                               String firstEditionPublisher, String firstEditionPublicationYear,
                                               String firstEditionPublicationPlace, String secondEditionPublisher,
                                               String secondEditionPublicationYear, String secondEditionPublicationPlace,
                                               String additionalInfos, String pageNumberInOCRDocument, String ocrResult,
                                               String correctionsAfter1938) {
        ShowBookFragment fragment = new ShowBookFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putBoolean("ssFlag", ssFlag);
        args.putString("authorFirstName", authorFirstName);
        args.putString("authorLastName", authorLastName);
        args.putString("firstEditionPublisher", firstEditionPublisher);
        args.putString("firstEditionPublicationYear", firstEditionPublicationYear);
        args.putString("firstEditionPublicationPlace", firstEditionPublicationPlace);
        args.putString("secondEditionPublisher", secondEditionPublisher);
        args.putString("secondEditionPublicationYear", secondEditionPublicationYear);
        args.putString("secondEditionPublicationPlace", secondEditionPublicationPlace);
        args.putString("additionalInfos", additionalInfos);
        args.putString("pageNumberInOCRDocument", pageNumberInOCRDocument);
        args.putString("ocrResult", ocrResult);
        args.putString("correctionsAfter1938", correctionsAfter1938);


        /*args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    public void updateView(Book book) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_show_book, container, false);
        title = (TextView) view.findViewById(R.id.tv_title_showbook);
        title.setText(getArguments().getString("title"));
        author = (TextView) view.findViewById(R.id.tv_author_showbook);
        author.setText(getArguments().getString("authorFirstName") + " " +
        getArguments().getString("authorLastName"));
        ssFlag = (TextView) view.findViewById(R.id.tv_ss_flag_showbook);
        if (getArguments().getBoolean("ssFlag"))
            ssFlag.setVisibility(View.VISIBLE);

        firstPublisher = (TextView) view.findViewById(R.id.tv_first_pub_showbook);
        firstPublisher.setText(getArguments().getString("firstEditionPublisher"));
        firstPublisherYear = (TextView) view.findViewById(R.id.tv_first_pub_year_showbook);
        firstPublisherYear.setText(getArguments().getString("firstEditionPublicationYear"));
        firstPublisherPlace = (TextView) view.findViewById(R.id.tv_first_pub_place_showbook);
        firstPublisherPlace.setText(getArguments().getString("firstEditionPublicationPlace"));
        secondPublisher = (TextView) view.findViewById(R.id.tv_sec_pub_showbook);
        secondPublisher.setText(getArguments().getString("secondEditionPublisher"));
        secondPublisherYear = (TextView) view.findViewById(R.id.tv_sec_pub_year_showbook);
        secondPublisherYear.setText(getArguments().getString("secondEditionPublicationYear"));
        secondPublisherPlace = (TextView) view.findViewById(R.id.tv_sec_pub_place_showbook);
        secondPublisherPlace.setText(getArguments().getString("secondEditionPublicationPlace"));
        addInfo = (TextView) view.findViewById(R.id.tv_add_info_showbook);
        addInfo.setText(getArguments().getString("additionalInfos"));
        pageNrOcr = (TextView) view.findViewById(R.id.tv_page_nr_showbook);
        pageNrOcr.setText(getArguments().getString("pageNumberInOCRDocument"));
        ocrResult = (TextView) view.findViewById(R.id.tv_ocr_result_showbook);
        ocrResult.setText(getArguments().getString("ocrResult"));
        corrections = (TextView) view.findViewById(R.id.tv_corrections_showbook);
        corrections.setText(getArguments().getString("correctionsAfter1938"));
        btnShowInDnb = (Button) view.findViewById(R.id.btn_show_in_dnb);
        btnShowInDnb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = getString(R.string.url_dnb);
                String title = getArguments().getString("title");
                title = title.replaceAll("\\s+","+").trim();
                String author = getArguments().getString("authorLastName");
                author = author.replaceAll("\\s+", "+").trim();
                url += title + "+" + author + getString(R.string.url_dnb_post_fix);
                Log.d("URL", url);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            }
        });
        return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);



        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
