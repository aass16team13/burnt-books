package de.berlin.htw.tb.burntbooks;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 * Test Book/FavList
 *
 */
public class ExampleUnitTest {
    @Test
    public void bookListCanBeCleared() throws Exception {
        BookList.clearBooks();
        assertEquals(BookList.books.size(), 0);
    }

    @Test
    public void booksCanBeAdded() throws Exception {
        BookList.clearBooks();
        BookList.addBook(new Book());
        assertEquals(BookList.books.size(), 1);
    }

    @Test
    public void booksCanBeRemoved() throws Exception {
        Book newBook = new Book();
        newBook.setTitle("TestTitle");
        newBook.setAuthorLastName("TestLastName");
        FavoritesList.clearBooks();
        FavoritesList.addBook(newBook);
        FavoritesList.removeBook(newBook);
        assertEquals(FavoritesList.books.size(), 0);
    }
}